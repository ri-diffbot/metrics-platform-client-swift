# WikimediaMetricsPlatform
Metrics client library for the official Wikipedia app for iOS, built in pure Swift to keep it FLOSS.

See the [Metrics Platform](https://wikitech.wikimedia.org/wiki/Metrics_Platform) project page on Wikitech for background.

## Testing
Run tests with `make test` and produce a coverage report with `make coverage`.

These commands will run in a temporary Docker container based on the official `swift` image.
